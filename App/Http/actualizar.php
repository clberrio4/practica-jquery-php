<?php 
	
    require_once '../Models/Query.php';
	require_once '../Models/Persona.php';

		if (!empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['edad']) && !empty($_POST['id'])) {
			$nombre=$_POST['nombre'];
			$apellido=$_POST['apellido'];
			$edad=$_POST['edad'];
			$id=$_POST['id'];

			$objperson= new Persona($id, $nombre, $apellido, $edad);
			$query= new Query();
			$query->actualizarUsuario($objperson);
		}else {
			echo json_encode([
				'response'=>False,
				'mensaje'=>'debes llenar todos los campos..!'
			]);
		}
 ?>