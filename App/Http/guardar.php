<?php 

	require_once '../Models/Query.php';
	require_once '../Models/Persona.php';

	if (!empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['edad'])) {
		$nombre=$_POST['nombre'];
		$apellido=$_POST['apellido'];
		$edad=$_POST['edad'];

		$objperson= new Persona(0, $nombre, $apellido, $edad);
		$query= new Query();
		$query->guardarUsuario($objperson);
	}else {
		echo json_encode([
			'response'=>False,
			'mensaje'=>'debes llenar todos los campos..!'
		]);
	}

 ?>