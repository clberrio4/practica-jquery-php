<?php 
require_once 'Conexion.php';
require_once 'Persona.php';	
/**
 * summary
 */
class Query extends Conexion
{
    /**
     * summary
     */
    public function __construct()
    {
        
    }

    public function guardarUsuario(Persona $persona){
    	$sql='INSERT INTO persona (nombre, apellido, edad) VALUES(?, ?, ?)';
    	$con=$this->getConection();

    	$stm=$con->prepare($sql);
    	$stm->execute([$persona->getNombre(), $persona->getApellido(), $persona->getEdad()]);
    	$resultado='El usuario: '.$persona->getNombre().' ha sido registrado';

    	echo json_encode([
    		'response'=>True,
    		'mensaje'=>$resultado
    	]);

    }

    public function listarUsuarios(){
    	$sql='SELECT * FROM persona';
    	$con=$this->getConection();

    	$stm=$con->prepare($sql);
    	$stm->execute();
    	$resultado=$stm->fetchAll(PDO::FETCH_OBJ);

    	echo json_encode([
    		'lista'=>$resultado
    	]);

    }

     public function actualizarUsuario(Persona $persona){
    	$sql='UPDATE persona SET nombre=:nombre, apellido=:apellido, edad=:edad WHERE id=:id' ;
    	$data=['nombre'=>$persona->getNombre(), 'apellido'=>$persona->getApellido(), 'edad'=>$persona->getEdad(),
    	 'id'=>$persona->getId()];
    	$con=$this->getConection();
    	$stm=$con->prepare($sql);
    	$stm->execute($data);
    	$resultado='El usuario: '.$persona->getNombre().' ha sido Actualizado';

    	echo json_encode([
    		'mensaje'=>$resultado
    	]);

    }

     public function eliminarUsuario($id){
    	$sql='DELETE FROM persona  WHERE id=:id';
    	$data=['id'=>$id];
    	$con=$this->getConection();
    	$stm=$con->prepare($sql);
    	$stm->execute($data);
    	$resultado='El usuario ha sido eliminado';

    	echo json_encode([
    		'mensaje'=>$resultado
    	]);

    }
}