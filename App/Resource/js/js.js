$(document).ready(function() {
	 $("#btnUp").hide();
	 $("#add").hide();

	automaticLoad();
	validarSave();

});
var personas;
function validarSave(){
	let btn = $("#btnsave");
	btn.click(function(event) {
	var valores = tomarValores();

		$.ajax({
			url: 'http://localhost/practica/App/Http/guardar.php',
			type: 'POST',
			dataType: 'json',
			data: valores,
			success: function(r){
				if(!r.response){
					asignarCampos(valores);
 						$.amaran({
					        'message'   :r.mensaje,
					        'stickyButton':true,
					        'closeOnClick':false,
					        'inEffect':'slideTop',
					        'outEffect':'slideLeft'
				  		  });
					 automaticLoad();
				}else{

					limpiarCampos();
					automaticLoad();
					 $.amaran({
				        'message'   :r.mensaje,
				        'stickyButton':true,
				        'closeOnClick':false,
				        'inEffect':'slideTop',
				        'outEffect':'slideLeft'
				    });
				}
			}
		})
		
	});
	

	
}
function tomarValores(){
	var nombre=$("input[name='nombre']").val();
	var apellido=$("input[name='apellido']").val();
	var edad=$("input[name='edad']").val();
	var id=$("input[name='id']").val();
	var valores={nombre, apellido, edad, id};
	
	return valores;
}

function limpiarCampos(){
	var nombre=$("input[name='nombre']").val("");
	var apellido=$("input[name='apellido']").val("");
	var edad=$("input[name='edad']").val("");
}

function asignarCampos(valores){
	var nombre=$("input[name='nombre']").val(valores.nombre);
	var apellido=$("input[name='apellido']").val(valores.apellido);
	var edad=$("input[name='edad']").val(valores.edad);

}

function automaticLoad(){
	$.ajax({
		url: '../../Http/listar.php',
		type: 'POST',
		dataType: 'json',
		data: {},
		success: function(r){
		  personas=r.lista;
		  fillTable();
		}
	})

}

function fillTable(){
	var tbody=$("#datatable");
	var data="";
	tbody.empty();
	$.each(personas, function(i, k) {
		var tr=`<tr>			
                    <th scope="row">${k.id}</th>
                    <td class="text-capitalize">${k.nombre}</td>
                    <td class="text-capitalize">${k.apellido}</td>
                    <td>${k.edad}</td>
                    <td class="text-center">
                            <button class=" btn btn-outline-warning" onclick="llenarCampoUpdate(${i})"><i class="fas fa-edit"></i></button>
                            <button class=" btn btn-outline-danger " onclick="eliminarPersona(${k.id})"><i class="fas fa-trash"></i></button>
                    </td>
				</tr>`;
		data+=tr;		
	});
	tbody.append(data);
}

 function llenarCampoUpdate(id){
 	$("#btnsave").hide();
 	$("#frm").show();
 	$("#btnUp").show();
 	volverArriba();
 	var filtro =personas[id];
 	var nombre=$("input[name='nombre']").val(filtro.nombre);
	var apellido=$("input[name='apellido']").val(filtro.apellido);
	var edad=$("input[name='edad']").val(filtro.edad);
	var id=$("input[name='id']").val(filtro.id);
	$("#add").hide();
	$("#closefrm").show();
 }

 function userUpdate(){
 	var valores=tomarValores();

 	$.ajax({
			url: 'http://localhost/practica/App/Http/actualizar.php',
			type: 'POST',
			dataType: 'json',
			data: valores,
			success: function(r){
				if(!r.response){
					console.log(r.mensaje);
					automaticLoad();
					limpiarCampos();
					$.amaran({
				        'message'           :r.mensaje,
				        'cssanimationIn'    :'shake',
				        'cssanimationOut'   :'fadeOutRight',
				        'outEffect'         :'slideRight',
				        'position'          :'top right'
  				   });

				}
			}
		})
 }
 function eliminarPersona(id){
 	var conf=confirm("are you sure to delete?");
 	if(conf){

 		$.ajax({
			url: 'http://localhost/practica/App/Http/eliminar.php',
			type: 'POST',
			dataType: 'json',
			data: {'codigo':id},
			success: function(r){
				
					  $.amaran({
				        'message'   :r.mensaje,
				        'stickyButton':true,
				        'closeOnClick':false,
				        'inEffect':'slideTop',
				        'outEffect':'slideLeft'
  					  });
					automaticLoad();

				}
			
		})
 	}else {
 		console.log('has elegido no eliminar');
 	}
 }

 	
 function volverArriba(){
 	$("html, body").animate({scrollTop: 0}, 'slow');
 	return false;
 }

 function mostarAgregar(){
 	limpiarCampos();
 	 $("#btnUp").hide();
 	 	$("#frm").show();
 	   $.amaran({content:{'message':'agregar un nuevo usuario!'}});
 	 $("#btnsave").show();
 	 $("#add").hide();	
 }
  function cerrarFormulario(){
  	$("#frm").hide();
  	$("#btnsave").hide();
  	$("#closefrm").hide();
  	$("#btnUp").hide();
  	$("#add").show();
  	
  }